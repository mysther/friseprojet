package Controleur;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;

import Modele.Evenement;
import Modele.Frise;
import Modele.MethodesFichier;
import Modele.ModeleTable;
import Vue.FenetreFrise;
import Vue.JDialogAjout;
import Vue.PanelAffichage;
import Vue.PanelCreation;
import Vue.PanelMain;

public class Controleur implements ActionListener, MouseListener {
	private PanelMain panelMain;
	private PanelAffichage panelAffichage;
	private PanelCreation panelCreation;
	private FenetreFrise fenetreFrise;
	
	private Frise frise;
	
	final JFileChooser fc;

	public Controleur(FenetreFrise parFenetreFrise ,PanelMain parPanelMain, PanelAffichage parPanelAffichage, PanelCreation parPanelCreation) {
		fenetreFrise = parFenetreFrise;
		panelMain = parPanelMain;
		panelAffichage = parPanelAffichage;
		panelCreation = parPanelCreation;
		
		
		panelMain.enregistreEcouteur(this);
		panelCreation.enregistreEcouteur(this);
		
		fc = new JFileChooser();
	    fc.setCurrentDirectory(new java.io.File("."));
		
	}

	public void actionPerformed(ActionEvent evt) {
		if(evt.getActionCommand().equals("boutOuvrirMain")) { 						//Appuis sur le bouton ouvrir depuis le panel Main
		    fc.setDialogTitle("Choississez la frise � ouvrir");
            fc.addChoosableFileFilter(new FileNameExtensionFilter("Fichiers Java", "ser"));
            fc.setAcceptAllFileFilterUsed(false);
			if (fc.showOpenDialog(panelMain) == JFileChooser.APPROVE_OPTION) {
	            fenetreFrise.setFrise((Frise) MethodesFichier.lecture(fc.getSelectedFile()));
	    		frise = fenetreFrise.getFrise();
	            frise.setAdrFichier(fc.getSelectedFile().toString());
				panelAffichage.chargementAffichageFrise(frise);
				panelAffichage.enregistreEcouteur(this);
				fenetreFrise.affichePanel("panelAffichage");
			}
		}
		else if(evt.getActionCommand().equals("boutCreeMain")) { 					//Appuis sur le bouton cr�ation depuis le panel Main
			fenetreFrise.affichePanel("panelCreation");
		}
		else if(evt.getActionCommand().equals("boutCreeCreation")) { 				//Appuis sur le bouton cr�ation depuis le panel Cr�ation
			if (panelCreation.estValide()) {
				fenetreFrise.setFrise(panelCreation.getFriseFromFormulaire());
	    		frise = fenetreFrise.getFrise();
				panelAffichage.chargementAffichageFrise(frise);
				panelAffichage.enregistreEcouteur(this);
				fenetreFrise.affichePanel("panelAffichage");
			}
		}
		else if(evt.getActionCommand().equals("boutRetourCreation")) { 				//Appuis sur le bouton Retour depuis le panel Cr�ation
			fenetreFrise.affichePanel("panelMain");
		}
		else if(evt.getActionCommand().equals("boutonSuppr")) {						//Appuis sur le bouton supprimer Evenement depuis le panel Diapo
			Evenement evenementSuppr = frise.del(panelAffichage.getPanelDiapo().getStringLabelOnTop());
			((ModeleTable) panelAffichage.getPanelFriseTableau().getTable().getModel()).delEvenement(evenementSuppr);
			panelAffichage.getPanelDiapo().reAddLabel(frise);
			
			MethodesFichier.ecriture(new File(frise.getAdrFichier()), frise);
		}
		
	}

	public void mouseClicked(MouseEvent evt) {
		JTable table = (JTable) evt.getSource();
		ModeleTable modele = (ModeleTable) table.getModel();
		Point point = evt.getPoint();
		int row = table.rowAtPoint(point);
		int column = table.columnAtPoint(point);
		if(modele.getValueAt(row, column) == null) {
			//Click sur une case vide
			JDialogAjout jDialogAjout = new JDialogAjout(null, "Ajoutez un �v�nement", true, frise.getAnneeDebut()+column, row);
			Evenement event = jDialogAjout.showJDialogAjout();
			if (event != null) {
				frise.add(event);
				MethodesFichier.ecriture(new File(frise.getAdrFichier()), frise);
				modele.addEvenement(event);
				panelAffichage.getPanelDiapo().reAddLabel(frise);
				panelAffichage.getPanelDiapo().afficheLabel(event.toString());
			}
		}
	}

	public void mouseEntered(MouseEvent arg0) {}

	public void mouseExited(MouseEvent arg0) {}

	public void mousePressed(MouseEvent arg0) {}

	public void mouseReleased(MouseEvent arg0) {}

}
