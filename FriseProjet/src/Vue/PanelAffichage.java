package Vue;

import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingConstants;

import Controleur.Controleur;
import Modele.Constante;
import Modele.Frise;
import Modele.ModeleTable;

public class PanelAffichage extends JPanel implements MouseListener, ActionListener {
	private static final long serialVersionUID = 1L;
	/**
	 * Panel qui contient le Diaporama d'�v�nements
	 */
	private PanelDiapo panelDiapo;
	/**
	 * Panel qui contient la frise sous forme de tableau.
	 */
	private PanelFriseTableau panelFriseTableau;
	
	/**
	 * <p>
	 * Construteur PanelAffichage � un corps vide car au moment de l'instanciation du panel, la frise � afficher
	 * n'est pas encore cr�e/choisie
	 * </p>
	 */
	public PanelAffichage() {}
	/**
	 * Constructeur qui prend en param�tre la frise et va instancier le diaporama et le tableau.
	 * @param parFrise
	 */
	public void chargementAffichageFrise(Frise parFrise) {
		
		panelDiapo = new PanelDiapo(parFrise);
		panelFriseTableau = new PanelFriseTableau(parFrise);
		panelFriseTableau.getTable().addMouseListener(this);
		
		JLabel labelNomFrise = new JLabel(parFrise.getTitre(), SwingConstants.CENTER);
		labelNomFrise.setFont(Constante.FONT_TITRE);
		
		panelDiapo.enregistreEcouteur(this);
		
		this.setLayout(new BorderLayout());
		this.add(labelNomFrise, BorderLayout.NORTH);
		this.add(panelDiapo, BorderLayout.CENTER);
		this.add(panelFriseTableau, BorderLayout.SOUTH);
	}

	/**
	 * Accesseur qui renvoit le panel contenant le Diaporama d'�v�nements
	 * @return panel contenant le Diaporama d'�v�nements
	 */
	public PanelDiapo getPanelDiapo() {
		return panelDiapo;
	}
	/**
	 * Accesseur qui renvoit le panel contenant la frise sous forme de tableau.
	 * @return panel contenant la frise sous forme de tableau.
	 */
	public PanelFriseTableau getPanelFriseTableau() {
		return panelFriseTableau;
	}
	/**
	 * M�thode qui se met � l'�coute des clics de la souris
	 */
	public void mouseClicked(MouseEvent evt) {
		JTable table = (JTable) evt.getSource();
		ModeleTable modele = (ModeleTable) table.getModel();
		Point point = evt.getPoint();
		int row = table.rowAtPoint(point);
		int column = table.columnAtPoint(point);
		if(modele.getValueAt(row, column) != null) {
			//Click sur une case o� un evenement est present
			panelDiapo.afficheLabel(modele.getValueAt(row, column).toString()); //affiche le labelEvenement en haut
		}
	}
	
	/**
	 * M�thode de la classe ActionListener : se met � l'�coute des �v�nements et les traite.
	 */
	public void actionPerformed(ActionEvent evt) {
		if(evt.getActionCommand().equals("boutonEst")) {
			panelDiapo.afficheNext();
			String labelStringKey = panelDiapo.getStringLabelOnTop();
			if(labelStringKey != null)
				panelFriseTableau.scrollToVisible(labelStringKey);
		}
		else if(evt.getActionCommand().equals("boutonOuest")) {
			panelDiapo.affichePrevious();
			String labelStringKey = panelDiapo.getStringLabelOnTop();
			if(labelStringKey != null)
				panelFriseTableau.scrollToVisible(labelStringKey);
		}
	}

	/**
	 * M�thode qui permet de cr�er la liaison directe entre le controleur et les boutons du panel. 
	 * @param parControleur - Le controleur qui va traiter l'�v�nement
	 */
	public void enregistreEcouteur(Controleur parControleur) {
		panelFriseTableau.getTable().addMouseListener(parControleur);
		panelDiapo.enregistreEcouteur(parControleur);
	}
	/**
	 * M�thode appel�e lorsque la souris entre dans un composant.
	 */
	public void mouseEntered(MouseEvent e) {}
	/**
	 * M�thode appel�e lorsque la souris sors d'un composant.
	 */
	public void mouseExited(MouseEvent e) {}
	/**
	 * M�thode appel�e lorsqu'un bouton de la souris est pr�ss�e sur un composant
	 */
	public void mousePressed(MouseEvent e) {}
	/**
	 * M�thode appel�e lorsqu'un bouton de la souris est relach� sur un composant.
	 */
	public void mouseReleased(MouseEvent e) {}

	
}