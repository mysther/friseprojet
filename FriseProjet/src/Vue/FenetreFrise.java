package Vue;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import Controleur.Controleur;
import Modele.Constante;
import Modele.Frise;

public class FenetreFrise extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	/**
	 * Correspond � la frise 
	 */
	private Frise frise;
	/**
	 * Correspond au layout utilis� pour l'arrangement des composants dans la fen�tre
	 */
	private CardLayout chCardLayout;
	/**
	 * Correspond au panel qui va contenir tous les �l�ments de la fen�tre.
	 */
	private JPanel contentPane;

	/**
	 * Cosntructeur de la fen�tre.
	 * @param parTitre - titre de la fen�tre, on passe le param�tre au constructeur de la classe JFrame.
	 */
	public FenetreFrise(String parTitre) {
		super(parTitre);
		
		contentPane = new JPanel();
		PanelMain panelMain = new PanelMain();
		PanelAffichage panelAffichage = new PanelAffichage();
		PanelCreation panelCreation = new PanelCreation();
		
		@SuppressWarnings("unused")
		Controleur controleur = new Controleur(this, panelMain,panelAffichage, panelCreation);
		
		chCardLayout = new CardLayout();
		contentPane.setLayout(chCardLayout);
		contentPane.add(panelMain);
		contentPane.add(panelAffichage);
		contentPane.add(panelCreation);
		chCardLayout.addLayoutComponent(panelMain, "panelMain");
		chCardLayout.addLayoutComponent(panelAffichage, "panelAffichage");
		chCardLayout.addLayoutComponent(panelCreation, "panelCreation");
		this.add(contentPane);
		
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("Fichier");
		JMenuItem exitMenu = new JMenuItem("Quitter");
		JMenu helpMenu = new JMenu("?");
		JMenuItem addMenu = new JMenuItem("Comment cr�er un �v�nement");
		JMenuItem proposMenu = new JMenuItem("� propos ...");
		
		menuBar.add(fileMenu);
		menuBar.add(helpMenu);
		fileMenu.add(exitMenu);
		helpMenu.add(addMenu);
		helpMenu.add(proposMenu);
		exitMenu.addActionListener(this);
		exitMenu.setActionCommand("exitMenu");
		addMenu.addActionListener(this);
		addMenu.setActionCommand("addMenu");
		proposMenu.addActionListener(this);
		proposMenu.setActionCommand("proposMenu");

        this.setJMenuBar(menuBar);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(1000,600);
        this.setLocation(100,100);
        this.setResizable(false);
        this.setVisible(true);
	}

	/**
	 * Accesseur de la frise 
	 * @return renvoit la frise.
	 */
	public Frise getFrise() {
		return frise;
	}

	/**
	 * Modificateur de la frise
	 * @param parFrise - nouvelle frise qui va remplacer l'ancienne.
	 */
	public void setFrise(Frise parFrise) {
		frise = parFrise;
	}
	
	/**
	 * M�thode qui permet d'afficher un panel en fonction de son nom.
	 * @param parNomPanel - nom du panel � afficher
	 */
	public void affichePanel(String parNomPanel) {
		chCardLayout.show(contentPane, parNomPanel);
	}
	/**
	 * M�thode statique main. Permet d'ouvrir la fen�tre m�re au lancement du programme.
	 * @param args
	 */
	public static void main(String[] args) {
		new FenetreFrise(Constante.NOM_APPLICATION);
	}

	/**
	 * M�thode de la classe ActionListener : se met � l'�coute des �v�nements et les traite.
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getActionCommand().equals("exitMenu"))
			System.exit(0);
		else if(arg0.getActionCommand().equals("addMenu"))
			JOptionPane.showMessageDialog(null, "Pour ajouter un �v�nement, cliquez dans le tableau � une case vide, un formulaire apparaitera", "Aide", JOptionPane.PLAIN_MESSAGE);
		else if(arg0.getActionCommand().equals("proposMenu")) 
			JOptionPane.showMessageDialog(null, "<html><div align=\"center\">Logiciel cr�� dans le cadre du semestre 2 DUT Info de l'IUT de V�lizy.<br><br>R�alis� par Danoffre Victor et Plouzin Zo�</div></html>", "Aide", JOptionPane.PLAIN_MESSAGE);
	}
}
