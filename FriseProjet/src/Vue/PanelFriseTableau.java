package Vue;

import java.awt.Dimension;
import java.awt.Rectangle;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;

import Modele.Frise;
import Modele.ModeleTable;

public class PanelFriseTableau extends JPanel {
	private static final long serialVersionUID = 1L;
	/**
	 * Correspond � la table de la frise
	 */
	private JTable table;
	/**
	 * Correspond aux donn�es de table
	 */
	private ModeleTable modele;
	/**
	 * Correspond � la barre d�filement
	 */
	private JScrollPane scrollPane;
	/**
	 * Construteur qui r�cup�re la frise, affiche les �v�nements dans le tableau et instancie le ScrollPane
	 * @param parFrise
	 */
	public PanelFriseTableau(Frise parFrise) {
		modele = new ModeleTable(parFrise);
		
		table = new JTable(modele);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.setRowHeight(65);
		table.setPreferredScrollableViewportSize(new Dimension(800,260));
		table.getTableHeader().setReorderingAllowed(false);
		table.getTableHeader().setResizingAllowed(false);
		table.setCellSelectionEnabled(false);
		
		scrollPane = new JScrollPane(table, ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		this.add(scrollPane);
	}
	/**
	 * Accesseur qui renvoit la table.
	 * @return la table 
	 */
	public JTable getTable() {
		return table;
	}
	/**
	 * fait d�filer la ScrollBar jusqu'� l'�v�nement dont la cl� est pass�e en param�tre
	 * @param parString
	 */
	public void scrollToVisible(String parString) {
		for (int col = 0 ; col < modele.getColumnCount() ; col++ ) {
			for (int row = 0 ; row < 4 ; row++) {
				if(modele.getValueAt(row, col) != null && modele.getValueAt(row, col).toString().equals(parString)) {
					Rectangle rect =  table.getCellRect(row, col, true);
					table.scrollRectToVisible(rect);
				}
			}
		}
	}
}
