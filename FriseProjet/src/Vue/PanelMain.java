package Vue;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Controleur.Controleur;
import Modele.Constante;

public class PanelMain extends JPanel {
	private static final long serialVersionUID = 1L;
	/**
	 * Bouton qui permet de cr�er une nouvelle frise
	 */
	private JButton boutCree = new JButton("Cr�er une nouvelle frise");
	/**
	 * Bouton qui permet d'ouvrir une frise
	 */
	private JButton boutOuvrir = new JButton("Ouvrir une frise");
	
	/**
	 * Constructeur qui affiche le menu de l'application avec les boutons permettant de cr�er ou ouvrir une frise.
	 */
	public PanelMain() {
		JLabel labelTitre = new JLabel(Constante.NOM_APPLICATION);
		labelTitre.setFont(Constante.FONT_TITRE);
		
		labelTitre.setAlignmentX(Component.CENTER_ALIGNMENT);
		boutCree.setAlignmentX(Component.CENTER_ALIGNMENT);
		boutOuvrir.setAlignmentX(Component.CENTER_ALIGNMENT);
		boutCree.setActionCommand("boutCreeMain");
		boutOuvrir.setActionCommand("boutOuvrirMain");
		
		JPanel panelImage = new JPanel();
		panelImage.setLayout(new BoxLayout(panelImage, BoxLayout.LINE_AXIS));
		panelImage.add(Box.createHorizontalGlue());
		panelImage.add(new JLabel(new ImageIcon("images/iut.png")));
		
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		this.add(Box.createRigidArea(new Dimension(0, 180)));
		this.add(labelTitre);
		this.add(Box.createRigidArea(new Dimension(0, 10)));
		this.add(boutCree);
		this.add(Box.createRigidArea(new Dimension(0, 10)));
		this.add(boutOuvrir);
		this.add(Box.createVerticalGlue());
		this.add(panelImage);
	}
	/**
	 * permet de cr�er la liaison directe entre le controleur et les boutons du panel. 
	 * @param parControleur - Le controleur qui va traiter l'�v�nement
	 */
	public void enregistreEcouteur(Controleur parControleur) {
		boutCree.addActionListener(parControleur);
		boutOuvrir.addActionListener(parControleur);		
	}
}