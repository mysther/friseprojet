package Vue;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Controleur.Controleur;
import Modele.Constante;
import Modele.Evenement;
import Modele.Frise;


public class PanelDiapo extends JPanel {
	private static final long serialVersionUID = 1L;
	/**
	 * bouton qui permet de passer � l'�v�nement pr�c�dent
	 */
	private JButton boutonOuest;
	/**
	 * bouton qui permet de passer � l'�v�nement suivant
	 */
	private JButton boutonEst;
	/**
	 * bouton qui permet de supprimer l'�v�nement
	 */
	private JButton boutonSuppr;
	/**
	 * Correspond au layout utilis� pour l'arrangement des composants dans la fen�tre
	 */
	private CardLayout chCardLayout;
	/**
	 * Panel contenant le diaporama des �v�nements
	 */
	private JPanel chPanelDiapo;
	/**
	 * Correspond � la frise
	 */
	private Frise chFrise;
	
	/**
	 * Constructeur qui affiche les labelEvenement de la frise par un CardLayout 
	 * @param parFrise
	 */
	public PanelDiapo(Frise parFrise){
		chFrise = parFrise;
		
		chCardLayout = new CardLayout();
		chPanelDiapo = new JPanel();
		chPanelDiapo.setLayout(chCardLayout);
		
		//Bouton d�placement du diapo
		boutonOuest = new JButton("<");
		boutonOuest.setBackground(Constante.COLOR_BACKGROUND);
		boutonOuest.setFont(Constante.FONT_TITRE);
		boutonOuest.setBorder(BorderFactory.createEmptyBorder(10,30,10,30));
		boutonOuest.setActionCommand("boutonOuest");
		boutonEst = new JButton(">");
		boutonEst.setBackground(Constante.COLOR_BACKGROUND);
		boutonEst.setFont(Constante.FONT_TITRE);
		boutonEst.setBorder(BorderFactory.createEmptyBorder(10,30,10,30));
		boutonEst.setActionCommand("boutonEst");
		
		//Bouton supprimer
		JPanel panelBouton = new JPanel();
		panelBouton.setLayout(new BoxLayout(panelBouton, BoxLayout.LINE_AXIS));
		boutonSuppr = new JButton("Supprimer l'�v�nement affich�");
		boutonSuppr.setActionCommand("boutonSuppr");
		panelBouton.add(Box.createRigidArea(new Dimension(75, 40)));
		panelBouton.add(boutonSuppr);
		panelBouton.add(Box.createHorizontalGlue());
		
		//Affichage des labels
		LabelEvenement label;
		for(Evenement evenement : chFrise.getTreeSetEvent()) {
			label=new LabelEvenement(evenement);
			label.setName(evenement.toString());
			chPanelDiapo.add(label);
			chCardLayout.addLayoutComponent(label, evenement.toString());
		}
		
		
		this.setLayout(new BorderLayout());
		this.add(boutonOuest, BorderLayout.WEST);
		this.add(boutonEst, BorderLayout.EAST);
		this.add(chPanelDiapo, BorderLayout.CENTER);
		this.add(panelBouton, BorderLayout.SOUTH);
	}
	/**
	 *affiche un LabelEvenement en fonction de son nom.
	 * @param parNomPanel
	 */
	public void afficheLabel(String parNomPanel) {
		chCardLayout.show(chPanelDiapo, parNomPanel);
	}
	/**
	 * renvoit le toString de l'�v�nement du label affich� 
	 * @return
	 */
	public String getStringLabelOnTop() {
		JLabel label = null;
		for (Component comp : chPanelDiapo.getComponents())
			if (comp.isVisible() == true)
				label = (JLabel) comp;
		return label.getName();
	}
	/**
	 * affiche le labelEvenement suivant
	 */
	public void afficheNext() {
		chCardLayout.next(chPanelDiapo);
	}
	/**
	 * affiche le labelEvenement pr�c�dent
	 */
	public void affichePrevious() {
		chCardLayout.previous(chPanelDiapo);
	}
	/**
	 * Met � jour les labels contenus dans la frise, utilis� lors de l'ajout ou la suppression d'un �v�nement
	 * @param parFrise
	 */
	public void reAddLabel(Frise parFrise) {
		chPanelDiapo.removeAll();
		chPanelDiapo.repaint(); // Utile dans le cas o� c'est le dernier label � supprimer, sinon il ne part pas
		LabelEvenement label;
		for(Evenement evenement : chFrise.getTreeSetEvent()) {
			label = new LabelEvenement(evenement);
			label.setName(evenement.toString());
			chPanelDiapo.add(label);
			chCardLayout.addLayoutComponent(label, evenement.toString());
		}
	}
	/**
	 * M�thode qui permet de cr�er une liaison directe entre les boutons du panelDiapo et le traitement d'�v�nements du panelAffichage. 
	 * @param parPanelAffichage- Le panel Affichage
	 */
	public void enregistreEcouteur(PanelAffichage parPanelAffichage) {
		boutonOuest.addActionListener(parPanelAffichage);
		boutonEst.addActionListener(parPanelAffichage);
	}
	/**
	 * M�thode qui permet de cr�er une liaison directe entre le controleur et les boutons du panelDiapo. 
	 * @param parControleur - Le controleur qui va traiter l'�v�nement
	 */
	public void enregistreEcouteur(Controleur parControleur) {
		boutonSuppr.addActionListener(parControleur);
	}
}
