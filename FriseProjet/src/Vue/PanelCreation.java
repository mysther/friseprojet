package Vue;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;

import Controleur.Controleur;
import Modele.Constante;
import Modele.Frise;

public class PanelCreation extends JPanel  implements ActionListener{
	private static final long serialVersionUID = 1L;
	/**
	 * Bouton de retour, sert � revenir en arri�re
	 */
	private JButton boutRetour;
	/**
	 * Bouton de cr�ation, permet de lancer la cr�ation de la frise
	 */
	private JButton boutCree;
	/**
	 * Bouton qui permet de lancer la popUp qui va permettre de parcourir les fichiers.
	 */
	private JButton boutParcourir;
	/**
	 * Correspond au label contenant le message d'erreur si l'utilisateur rentre mal les informations.
	 */
	private JLabel labelErreur = new JLabel("");
	/**
	 * Correspond au label affichant le chemin du dossier selectionn�, en dessous du boutParcourir.
	 */
	private JLabel labelCheminDossier;
	/**
	 * Champ de texte o� l'utilisateur rentre le titre de l'�v�nement
	 */
	private JTextField textTitreFrise;
	/**
	 * Champ de texte o� l'utilisateur rentre l'ann�e de d�but de l'�v�nement
	 */
	private JTextField textAnneeDebut;
	/**
	 * Champ de texte o� l'utilisateur rentre l'ann�e de fin de l'�v�nement
	 */
	private JTextField textAnneeFin;
	/**
	 * Champ de texte o� l'utilisateur rentre la p�riode de l'�v�nement
	 */
	private JTextField textPeriode;
	/**
	 * Chaine de caracteres correspondant au chemin du dossier s�lectionn�
	 */
	private String chCheminDossier;
	/**
	 * PopUp qui permet de parcourir les fichiers et d'en selectionner un
	 */
	private JFileChooser fc;
	
	/**
	 * Constructeur du panel cr�ation, instancie le formulaire de cr�ation de la frise
	 */
	public PanelCreation(){
		JPanel panelFormulaire = new JPanel();
		panelFormulaire.setLayout(new GridBagLayout());
		panelFormulaire.setMaximumSize(new Dimension(500, 0));
	    panelFormulaire.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED)));
		
	    textTitreFrise = new JTextField(10);
	    textAnneeDebut = new JTextField(10);
	    textAnneeFin = new JTextField(10);
	    textPeriode = new JTextField(10);

		JLabel labelTitreFrise = new JLabel("Titre de la frise : ");
		JLabel labelEnregistrer = new JLabel("Dossier o� enregistrer : "); //http://www.rgagnon.com/javadetails/java-0370.html
		JLabel labelAnneeDebut = new JLabel("Ann�e de d�but : ");
		JLabel labelAnneeFin = new JLabel("Ann�e de fin : ");
		JLabel labelPeriode = new JLabel("P�riode : ");
		
		//Mn�monique
		labelTitreFrise.setDisplayedMnemonic('T');
		labelTitreFrise.setLabelFor(textTitreFrise);
		labelAnneeDebut.setDisplayedMnemonic('d');
		labelAnneeDebut.setLabelFor(textAnneeDebut);
		labelAnneeFin.setDisplayedMnemonic('f');
		labelAnneeFin.setLabelFor(textAnneeFin);
		labelPeriode.setDisplayedMnemonic('P');
		labelPeriode.setLabelFor(textPeriode);
		
		labelCheminDossier = new JLabel("Vous n'avez pas selectionn� de dossier de sauvegarde");
		labelCheminDossier.setBackground(Constante.COLOR_ERREUR);
		labelCheminDossier.setOpaque(true);
	    
		boutRetour = new JButton("Retour");
		boutRetour.setActionCommand("boutRetourCreation");
		boutCree = new JButton("Cr�er une nouvelle frise");
		boutCree.setActionCommand("boutCreeCreation");
		boutParcourir = new JButton("Parcourir");
		boutParcourir.setActionCommand("boutParcourir");
		boutParcourir.setPreferredSize(new Dimension(113, 22));
		boutParcourir.addActionListener(this);
		
		fc = new JFileChooser();
	    fc.setCurrentDirectory(new java.io.File("."));
	    fc.setDialogTitle("Choissisez un repertoire o� sauvegarder la frise");
	    fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	    fc.setAcceptAllFileFilterUsed(false);  //enleve le filtre du choix d'extention des fichiers
		
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.LINE_START;
		c.insets = new Insets(4,4,4,4);

		panelFormulaire.add(Box.createRigidArea(new Dimension(0, 10)), c);
		c.gridy=1;
	    panelFormulaire.add(labelTitreFrise, c);
		c.gridy=2;
	    panelFormulaire.add(labelAnneeDebut, c);
	    
		c.gridy=3;
	    panelFormulaire.add(labelAnneeFin, c);
	    
		c.gridy=4;
	    panelFormulaire.add(labelPeriode, c);

		c.gridy=5;
	    panelFormulaire.add(labelEnregistrer, c);
	    
		c.gridy=6;
		c.gridwidth=2;
	    panelFormulaire.add(labelCheminDossier, c);
	    
	    c.gridx=1;
	    c.gridy=1;
		c.gridwidth=1;
	    panelFormulaire.add(textTitreFrise, c);

	    c.gridy=2;
	    panelFormulaire.add(textAnneeDebut, c);

	    c.gridy=3;
	    panelFormulaire.add(textAnneeFin, c);

	    c.gridy=4;
	    panelFormulaire.add(textPeriode, c);

	    c.gridy=5;
	    panelFormulaire.add(boutParcourir, c);
	    
	    c.gridy=7;
		panelFormulaire.add(Box.createRigidArea(new Dimension(0, 10)), c);
		

		JPanel panelBouton = new JPanel();
		panelBouton.setLayout(new BoxLayout(panelBouton, BoxLayout.LINE_AXIS));
		panelBouton.add(Box.createHorizontalGlue());
		panelBouton.add(boutRetour);
		panelBouton.add(Box.createRigidArea(new Dimension(10, 0)));
		panelBouton.add(boutCree);
		panelBouton.add(Box.createHorizontalGlue());
		
		JLabel labelTitre = new JLabel("Cr�ation d'une nouvelle frise");
		labelTitre.setFont(Constante.FONT_TITRE);
		labelTitre.setAlignmentX(CENTER_ALIGNMENT);
		
		labelErreur.setForeground(Color.RED);
		labelErreur.setAlignmentX(CENTER_ALIGNMENT); 
	    
	    this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		this.add(Box.createVerticalGlue());
		
		this.add(labelTitre);
		this.add(Box.createRigidArea(new Dimension(0, 20)), c);
	    this.add(panelFormulaire);
		this.add(Box.createRigidArea(new Dimension(0, 20)), c);
		this.add(panelBouton);
		this.add(Box.createRigidArea(new Dimension(0, 10)), c);
		this.add(labelErreur);
	    
		this.add(Box.createVerticalGlue());
	}
	/**
	 * M�thode qui permet de cr�er la liaison directe entre le controleur et les boutons du panel. 
	 * @param parControleur - Le controleur qui va traiter l'�v�nement
	 */
	public void enregistreEcouteur(Controleur parControleur) {
		boutCree.addActionListener(parControleur);
		boutRetour.addActionListener(parControleur);
	}
	/**
	 * M�thode qui permet de cr�er la liaison directe entre le controleur et les boutons du panel. 
	 * @param parControleur - Le controleur qui va traiter l'�v�nement
	 */
	public boolean estValide() {
		if(textTitreFrise.getText().equals("") || textTitreFrise.getText().equals(" "))  {
			labelErreur.setText("Veuillez entrer un titre pour la frise !");
			return false;
		}
		try {
			Integer.parseInt(textAnneeDebut.getText());
			Integer.parseInt(textAnneeFin.getText());
			Integer.parseInt(textPeriode.getText());
		}
		catch (NumberFormatException nfe) {
			labelErreur.setText("Veuillez entrer un entier dans les champs ann�es de d�but/fin et p�riode !");
			return false;
		}
		
		if(Integer.parseInt(textAnneeDebut.getText()) > Integer.parseInt(textAnneeFin.getText())) {
			labelErreur.setText("L'ann�e de d�but doit �tre inf�rieure � l'ann�e de fin!");
			return false;
		}
		else if (Integer.parseInt(textPeriode.getText())<=0) {
			labelErreur.setText("Veuillez entrer une p�riode sup�rieure ou �gale � 1 !");
			return false;
		}
		else if (chCheminDossier == null) {
			labelErreur.setText("Veuillez choisir un dossier de sauvegarde");
			return false;
		}
		else {
			labelErreur.setText("");
			return true;
		}
	}
	
	/**
	 * M�thode qui renvoit une frise cr��e � partir du formulaire.
	 * @return
	 */
	public Frise getFriseFromFormulaire() {
		return new Frise(Integer.parseInt(textAnneeDebut.getText()), Integer.parseInt(textAnneeFin.getText()), Integer.parseInt(textPeriode.getText()), textTitreFrise.getText(), chCheminDossier.concat(File.separator + textTitreFrise.getText()+".ser"));
	}
	/**
	 * M�thode de la classe ActionListener : se met � l'�coute des �v�nements et les traite.
	 */
	public void actionPerformed(ActionEvent evt) {
		if(evt.getActionCommand().equals("boutParcourir")) {
			if(fc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
				this.chCheminDossier = fc.getSelectedFile().toString();
				this.labelCheminDossier.setText(chCheminDossier);
				this.labelCheminDossier.setBackground(Constante.COLOR_OK);
			}
		}
	}
			
}
