package Vue;

import java.awt.Image;
import java.util.StringTokenizer;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import Modele.Evenement;

public class LabelEvenement extends JLabel {
	private static final long serialVersionUID = 1L;
	/**
	 * Permet de mettre en forme l'affichage de l'évéénement dans le diaporama
	 * @param evenement - événement selectionné
	 */
	public LabelEvenement(Evenement evenement) {
		String titre = new String("<h2>"+evenement.getTitre()+"</h2>");
		String date = new String("<em>"+evenement.getChDate().toString()+"</em>");
		String desc = new String("<p>");
		StringTokenizer st = new StringTokenizer(evenement.getDescription());
		int i = 0;
		String s;
		while(st.hasMoreTokens() && i <= 60){
			s = st.nextToken();
			desc = desc.concat(s).concat(" ");
			i++;
		}
		if(st.hasMoreTokens())
			desc = desc.concat("<em> (...)</em>");
		desc = desc.concat("</p>");
		this.setIcon(new ImageIcon(evenement.getImage().getImage().getScaledInstance(200, 200, Image.SCALE_DEFAULT)));
		this.setText("<html>"+ titre + date +"<br><br>" +desc +"</html>");
	}
}
