package Vue;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import Modele.Constante;
import Modele.Date;
import Modele.Evenement;

public class JDialogAjout extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	/**
	 * Correspond au label affichant le chemin de l'image selectionn�e, en dessous des champs du formulaire.
	 */
	private JLabel labelCheminImage;
	/**
	 * Correspond au label contenant le message d'erreur si l'utilisateur rentre mal les informations.
	 */
	private JLabel labelErreur;
	/**
	 * Bouton pour effectuer l'ajoute de l'�v�nement.
	 */
	private JButton boutAjout;
	/**
	 * Bouton pour annuler la saisie de l'�v�nement.
	 */
	private JButton boutAnnuler;
	/**
	 * Bouton pour parcourir les fichiers afin de trouver l'image.
	 */
	private JButton boutParcour;
	/**
	 * PopUp qui permet de parcourir les fichiers et d'en selectionner un
	 */
	private JFileChooser fc;
	/**
	 * Champ de texte o� l'utilisateur rentre le titre de l'�v�nement
	 */
	private JTextField textTitre;
	/**
	 * Champ de texte o� l'utilisateur rentre l'an�e de l'�v�nement
	 */
	private JTextField textAn;
	/**
	 * Champ de texte o� l'utilisateur rentre le jour de l'�v�nement
	 */
	private JTextField textJour;
	/**
	 * Champ de texte o� l'utilisateur rentre le mois de l'�v�nement
	 */
	private JTextField textMois;
	/**
	 * Champ de texte o� l'utilisateur rentre la description de l'�v�nement
	 */
	private JTextArea textDesc;
	/**
	 * ComboBox contenant les valeurs possibles de l'importance, c'est-�-dire de 1 � 4
	 */
	private JComboBox<String> boxImport;
	/**
	 * Chaine de caract�res correspondant � l'adresse de l'image.
	 */
	private String adrImg = new String();
	/**
	 * Correspond � l'�v�nement que l'on va construire
	 */
	private Evenement evenement;
	/**
	 * Correspond � l'ann�e de l'�v�nement, qui est pr�-remplie
	 */
	private int annee;
	/**
	 * Correspond � l'importance de l'�v�nement, qui est pr�-remplie
	 */
	private int importance;

	/**
	 * PopUp qui permet d'ajouter un �v�nement lorsqu'on clique sur une cellule de la frise, les champs correspondants � l'importance et � l'ann�e sont pr�-rempli en fonction de la cellule s�lectionn�e
	 * @param parent - La fen�tre de laquelle d�pend le popUp, ici sa valeur restera "null". 
	 * @param title - Titre du popUp
	 * @param modal - Si true, l'acc�s aux autres fen�tre est bloqu�, si false on peux acc�der aux autre fen�tres, ici on laisse sa valeur � true.
	 * @param parAnnee - ann�e de l'�v�nement, r�cup�r�e lors du clic que la souris.
	 * @param parImportance - importance de l'�v�nement, r�cup�r�e lors du clic que la souris.
	 */
	public JDialogAjout(JFrame parent, String title, boolean modal, int parAnnee, int parImportance) {
		super(parent, title, modal);
		annee = parAnnee;
		importance = parImportance;
		
        this.setSize(550,450);
        this.setLocation(350,200);
        this.setResizable(true);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.initComponent();
	}
	/**
	 * M�thode permettant d'afficher le popUp et qui renvoit l'�v�nement cr��
	 * @return renvoit l'�v�nement cr��
	 */
	public Evenement showJDialogAjout() {
		this.setVisible(true);
		return evenement;
	}

	/**
	 * M�thode qui construit le formulaire de saisie des informations utiles pour cr�er l'�v�nement.
	 */
	private void initComponent() {
		fc = new JFileChooser();
	    fc.setCurrentDirectory(new java.io.File("."));
	    fc.setDialogTitle("Selectionner une image : ");
        fc.addChoosableFileFilter(new FileNameExtensionFilter("Fichier png", "png"));
        fc.addChoosableFileFilter(new FileNameExtensionFilter("Fichier jpeg", "jpeg"));
        fc.addChoosableFileFilter(new FileNameExtensionFilter("Fichier jpg", "jpg"));
	    
		JPanel panelFormulaire = new JPanel();
		panelFormulaire.setMaximumSize(new Dimension(450, 450));
		panelFormulaire.setLayout(new GridBagLayout());
		
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.LINE_START;
		c.insets = new Insets(4,4,4,4);
		
		String valImport[]= {"4","3","2","1"};
		
		JLabel labelAjout = new JLabel("Ajout d'un �v�nement");
		labelAjout.setFont(Constante.FONT_TITRE);
		labelAjout.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		
		textTitre = new JTextField(10);
		textAn = new JTextField(10);
		textAn.setText(Integer.toString(annee));
		textMois = new JTextField(10);
		textJour = new JTextField(10);
		boxImport = new JComboBox<String>(valImport);
		boxImport.setSelectedIndex(importance);
		textDesc = new JTextArea(5,11);
		textDesc.setLineWrap(true);
		textDesc.setWrapStyleWord(true);
		
		JLabel labelTitre = new JLabel("Titre : ");
		JLabel labelAn = new JLabel("Ann�e (AAAA) : ");
		JLabel labelMois = new JLabel("Mois (MM) : ");
		JLabel labelJour = new JLabel("Jour (JJ) : ");
		JLabel labelImport = new JLabel("Importance (1-4) :");
		JLabel labelAdrImg = new JLabel("Adresse image :");
		JLabel labelDesc = new JLabel("Description : ");
		
		//Mn�monique
		labelTitre.setDisplayedMnemonic('T');
		labelTitre.setLabelFor(textTitre);
		labelAn.setDisplayedMnemonic('A');
		labelAn.setLabelFor(textAn);
		labelMois.setDisplayedMnemonic('M');
		labelMois.setLabelFor(textMois);
		labelJour.setDisplayedMnemonic('J');
		labelJour.setLabelFor(textJour);
		labelImport.setDisplayedMnemonic('I');
		labelImport.setLabelFor(boxImport);		
		labelDesc.setDisplayedMnemonic('D');
		labelDesc.setLabelFor(textDesc);
		
		
		labelCheminImage = new JLabel("Vous n'avez pas mis d'image");
		labelCheminImage.setBackground(Constante.COLOR_ERREUR);
		labelCheminImage.setOpaque(true);
		labelErreur = new JLabel("");
		labelErreur.setForeground(Color.RED);

		boutAjout = new JButton("Ajouter");
		boutAjout.setActionCommand("boutAjout");
		boutAjout.addActionListener(this);
		boutAnnuler = new JButton("Annuler");
		boutAnnuler.setActionCommand("boutAnnuler");
		boutAnnuler.addActionListener(this);
		boutParcour = new JButton("Parcourir");
		boutParcour.setActionCommand("boutParcour");
		boutParcour.addActionListener(this);
		
		
		JScrollPane areaScrollPane = new JScrollPane(textDesc, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		panelFormulaire.add(labelAjout,c);
		c.gridy=1;
		panelFormulaire.add(labelTitre,c);
		c.gridy=2;
		panelFormulaire.add(labelAn,c);
		c.gridy=3;
		panelFormulaire.add(labelMois,c);
		c.gridy=4;
		panelFormulaire.add(labelJour,c);
		c.gridy=5;
		panelFormulaire.add(labelImport,c);
		c.gridy=6;
		panelFormulaire.add(labelDesc,c);
		c.gridy=7;
		panelFormulaire.add(labelAdrImg,c);

		c.gridwidth=2;
		c.gridy=8;
		panelFormulaire.add(labelCheminImage,c);
		c.gridy=9;
		panelFormulaire.add(labelErreur,c);
		
		c.gridwidth=1;
		c.gridx=1;
		c.gridy=1;
		panelFormulaire.add(textTitre,c);
		c.gridy=2;
		panelFormulaire.add(textAn,c);
		c.gridy=3;
		panelFormulaire.add(textMois,c);
		c.gridy=4;
		panelFormulaire.add(textJour,c);
		c.gridy=5;
		panelFormulaire.add(boxImport,c);
		c.gridy=6;
		panelFormulaire.add(areaScrollPane,c);
		c.gridy=7;
		panelFormulaire.add(boutParcour,c);
		
		

		JPanel panelBouton = new JPanel();
		panelBouton.setLayout(new BoxLayout(panelBouton, BoxLayout.LINE_AXIS));
		panelBouton.add(Box.createHorizontalGlue());
		panelBouton.add(boutAnnuler);
		panelBouton.add(Box.createRigidArea(new Dimension(10, 0)));
		panelBouton.add(boutAjout);
		panelBouton.add(Box.createRigidArea(new Dimension(10, 0)));
		
		JPanel contentPane = new JPanel();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));
		contentPane.add(Box.createVerticalGlue());
		contentPane.add(panelFormulaire);
		contentPane.add(Box.createRigidArea(new Dimension(0, 10)), c);
		contentPane.add(panelBouton);
		contentPane.add(Box.createVerticalGlue());
		this.add(contentPane);
	}
	
	/**
	 * M�thode qui renvoit un bool�en permettant de v�rifier si l'�v�nement construit est valide ou non.
	 * @return renvoit true si l'�v�nement construit est bien valide et false si un champ est mal renseign�.
	 */
	public boolean estValide() {
		if(textTitre.getText().equals("") || textTitre.getText().equals(" ")) {
			labelErreur.setText("Veuillez entrer un titre pour l'�v�nement");
			return false;
		}	
		try {
			Integer.parseInt(textAn.getText());
			Integer.parseInt(textMois.getText());
			Integer.parseInt(textJour.getText());
		}
		catch (NumberFormatException nfe) {
			labelErreur.setText("Veuillez entrer des valeurs num�riques pour l'ann�e, le mois et le jour");
			return false;
		}
		
		Date dateTest = new Date(Integer.parseInt(textJour.getText()),Integer.parseInt(textMois.getText()),Integer.parseInt(textAn.getText()));
		if (!dateTest.estValide()) {
			labelErreur.setText("Veuillez entrer une date valide");
			return false;
		}
		return true;
	}

	/**
	 * M�thode de la classe ActionListener : se met � l'�coute des �v�nements et les traite.
	 */
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getActionCommand().equals("boutParcour")){
			if(fc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
				this.adrImg = fc.getSelectedFile().toString();
				this.labelCheminImage.setText(adrImg);
				labelCheminImage.setBackground(Constante.COLOR_OK);
			}
		}
		else if(arg0.getActionCommand().equals("boutAjout")) {
			if(estValide()){
				int jour = Integer.parseInt(textJour.getText());
				int mois = Integer.parseInt(textMois.getText());
				int annee = Integer.parseInt(textAn.getText());
				evenement = new Evenement(textTitre.getText(), new Date(jour, mois, annee), textDesc.getText(), adrImg, boxImport.getSelectedIndex()+1);
				setVisible(false);
			}
			
		}
		else if(arg0.getActionCommand().equals("boutAnnuler")) {
			setVisible(false);
		}
	}

}
