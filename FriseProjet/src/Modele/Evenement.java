package Modele;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class Evenement implements Comparable <Evenement>, Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * Indique le titre de l'�v�nement
	 */
	private String chTitre;
	
	/**
	 * Indique la description de l'�v�nement
	 */
	private String chDescription;
	
	/**
	 * Indique l'image de l'�v�nement
	 */
	private ImageIcon chImageEvenement;
	
	/**
	 * Indique le poids de l'�v�nement (sa position verticale dans le tableau)
	 */
	private int chImportance;
	
	/**
	 * Indique la date de l'�v�nement
	 */
	private Date chDate;
	
	/**
	 * Constructeur d'un �v�nement
	 * @param parTitre Param�tre r�cuperant le titre de l'�v�nement
	 * @param parDate Param�tre r�cuperant la date de l'�v�nement
	 * @param parDescription Param�tre r�cuperant la description de l'�v�nement
	 * @param parAdrImg Param�tre r�cuperant le titre de l'�v�nement
	 * @param parImportance Param�tre r�cuperant le poids de l'�v�nement
	 */
	public Evenement(String parTitre,Date parDate, String parDescription, String parAdrImg, int parImportance) {
		chTitre = parTitre;
		chDescription = parDescription;
		try {
			chImageEvenement = new ImageIcon(ImageIO.read(new File(parAdrImg)));
		} catch (IOException e) {
			System.out.println("Image non valide");
			chImageEvenement = new ImageIcon("images/question_mark.png");
			//chImageEvenement = new ImageIcon("friseProjet/images/question_mark.png");
		}
		chImportance = parImportance;
		chDate = parDate;
	}

	/**
	 * Accesseur qui renvoit le titre de l'�v�nement
	 * @return Renvoit le titre de l'�v�nement
	 */
	public String getTitre(){
		return chTitre;
	}
	
	/**
	 * Accesseur qui renvoit l'image de l'�v�nement
	 * @return Renvoit l'image de l'�v�nement
	 */
	public ImageIcon getImage() {
		return chImageEvenement;
	}
	
	/**
	 * Accesseur qui renvoit le poids de l'�v�nement 
	 * @return Renvoit le poids de l'�v�nement
	 */
	public int getImportance() {
		return chImportance;
	}
	
	/**
	 * Accesseur qui renvoit la date de l'�v�nement
	 * @return Renvoit la date de l'�v�nement
	 */
	public Date getChDate() {
		return chDate;
	}
	
	/**
	 * Accesseur qui renvoit la description de l'�v�nement
	 * @return Renvoit la description de l'�v�nement
	 */
	public String getDescription() {
		return chDescription;
	}

	/**
	 * Appelle la fonction compareTo de Date avec les champs chDate des �v�nements
	 * @see Modele.Date.compareTo()
	 * @return Renvoit exatement le compareTo() entre les deux dates.
	 */
	public int compareTo(Evenement parEvenement) {
		return this.chDate.compareTo(parEvenement.getChDate());
	}
}
