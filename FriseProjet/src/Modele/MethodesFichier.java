package Modele;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class MethodesFichier {
	/**
	 * Lit un fichier .ser et le convertit en Object
	 * @param parFichier File - le fichier .ser
	 * @return Object recuper� depuis le fichier .ser
	 */
	public static Object lecture (File parFichier){
		ObjectInputStream flux;
		Object objetLu = null;
		
		try {
			flux = new ObjectInputStream(new FileInputStream(parFichier));
			objetLu = (Object)flux.readObject();
			flux.close();
		}
		catch(ClassNotFoundException parException){
			System.err.println(parException.toString());
			System.exit(1);
		}
		catch(IOException parException){
			System.err.println ("Erreur lecture du fichier " + parException.toString ());
			System.exit (1);
		}
		return objetLu;
	}
	
	/**
	 * Ecrit un fichier .ser depuit une class Object
	 * @param parFichier File - le fichier .ser � �crire
	 * @param parObjet Object qui sera �crit dans le .ser
	 */
	public static void ecriture (File parFichier, Object parObjet) {
		ObjectOutputStream flux = null;
		
		try{
			flux = new ObjectOutputStream (new FileOutputStream (parFichier));
			flux.writeObject (parObjet);
			flux.flush ();
			flux.close ();
		}
		catch(IOException parException){
			System.err.println ("Probleme a l�ecriture\n" + parException.toString ());
			System.exit (1);
		}
	}

}
