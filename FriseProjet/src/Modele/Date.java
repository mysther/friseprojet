package Modele;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Date implements Comparable<Date>, Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Indique le jour de la date
	 */
	private int chJour;
	
	/**
	 * Indique le mois de la date
	 */
	private int chMois;
	
	/**
	 * Indique l'ann�e de la date
	 */
	private int chAnnee;
	
	/**
	 * Indique le num�ro du jour de la date
	 */
	private int jourSemaine;
	
	/**
	 * Construit une date qui est initialis�e avec les param�tres donn�s
	 * @param parJour initialise le jour de la date
	 * @param parMois initialise le mois de la date
	 * @param parAnnee initialise l'ann�e de la date
	 */
	public Date (int parJour, int parMois, int parAnnee) {
		chJour=parJour;
		chMois=parMois;
		chAnnee=parAnnee;
		
		GregorianCalendar aujourdhui=new GregorianCalendar(chAnnee, chMois -1, chJour);
		jourSemaine=aujourdhui.get(Calendar.DAY_OF_WEEK);
	}
	
	/**
	 * Construit une Date qui est initialis�e par d�faut avec la date du jour
	 */
	public Date(){
		GregorianCalendar aujourdhui= new GregorianCalendar();
		chAnnee=aujourdhui.get(Calendar.YEAR);
		chMois=aujourdhui.get(Calendar.MONTH)+1;
		chJour=aujourdhui.get(Calendar.DAY_OF_MONTH);
		jourSemaine=aujourdhui.get(Calendar.DAY_OF_WEEK);
	}
	
	/**
	 * M�thode statique permettant de renvoyer le dernier jour d'un mois
	 * @param parMois indique le mois concern�
	 * @param parAnnee indique l'ann�e concern�e
	 * @return renvoie un entier avec le dernier jour du mois
	 */
	public static int dernierJourDuMois (int parMois, int parAnnee) {
		switch (parMois){
			case 2 :
				if (estBissextile(parAnnee)){
					return 29;
				}
				else {
					return 28;
				}
			case 4:case 6:case 9:case 11:
				return 30;
			default:
				return 31;
		}
	}
	
	/**
	 * M�thode statique renvoyant si une ann�e est bissextile ou non
	 * @param parAnnee indique l'ann�e concern�e
	 * @return Renvoie true si l'ann�e est bissextile, sinon false
	 */
	public static boolean estBissextile(int parAnnee) {
		return (parAnnee % 400 == 0) || ((parAnnee % 4 == 0) && (parAnnee % 100 != 0));
	}
	
	//true si valide, false si non valide (mois entre 1 et 12, année > 1582 et jour entre 1 et dernier jour du mois)
	/**
	 * M�thode indiquant si une date est valide ou non
	 * @return Renvoie true si la date est valide, sinon false
	 */
	public boolean estValide() {
		int dernierJour = dernierJourDuMois(chMois, chAnnee);
		if ((chAnnee >= 1583) && (chMois >= 1) && (chMois <= 12) && (chJour >= 1) && (chJour <= dernierJour))
			return true;
		else 
			return false;
	}
	
	/**
	 * Compare deux dates chronologiquement
	 * @param parDate indique la date utilis�e pour la comparaison
	 * @return Renvoie un entier n�gatif si This est ant�rieur au param�tre, positif si This est post�rieur au param�tre, renvoie 0 si les dates sont identiques.
	 */
	public int compareTo (Date parDate) {
		int resultat;
		
		if (parDate.chAnnee>this.chAnnee){
			resultat = -1;
		}
		
		else if (parDate.chAnnee<this.chAnnee) {
			resultat = 1;
		}
		
		else {
			if (parDate.chMois>this.chMois){
				resultat= -1;
			}
			else if (parDate.chMois<this.chMois){
				resultat=1;
			}
			else {
				if (parDate.chJour>this.chJour){
					resultat=-1;
				}
				else if (parDate.chJour<this.chJour){
					resultat=1;
				}
				else {
					resultat=0;
				}
			}
		}
	return resultat;
	}
	
	/**
	 * Accesseur qui renvoit le jour de la date
	 * @return Renvoit un entier contenant le jour de la date
	 */
	public int getJour() {
		return chJour;
	}
	
	/**
	 * Accesseur qui renvoit le mois de la date
	 * @return Renvoit un entier contenant le mois de la date
	 */
	public int getMois() {
		return chMois;
	}
	
	/**
	 * Accesseur qui renvoit l'ann�e de la date
	 * @return Renvoit un entier contenant l'ann�e de la date
	 */
	public int getAnnee() {
		return chAnnee;
	}
	
	/**
	 * Renvoit la date sous la forme NOM_DU_JOUR  JOUR   NOM_DU_MOIS  ANNEE
	 * @return Renvoit un entier contenant l'ann�e de la date
	 */
	public String toString() {
		return Constante.TAB_STR_JOURS[jourSemaine-1]+" "+chJour+" "+ Constante.TAB_STR_MOIS[chMois-1] + " " +chAnnee;
	}
}