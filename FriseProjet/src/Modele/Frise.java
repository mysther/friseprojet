package Modele;

import java.io.Serializable;
import java.util.TreeSet;

public class Frise implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * Indique le titre de la frise
	 */
	private String chTitre;
	
	/**
	 * Indique l'ann�e de d�but de la frise
	 */
	private int chAnneeDebut;
	
	/**
	 * Indique l'ann�e de fin de la frise
	 */
	private int chAnneeFin;
	
	/**
	 * Indique l'�cart entre deux intitul�s de colonnes de la JTable de PanelFriseTableau
	 */
	private int chPeriode;
	/**
	 * Indique le nombre d'�v�nement(s) dans la frise
	 */
	private int chNombreEvenement;
	
	/**
	 * Indique o� le fichier est sauvegard�, utilis� dans les sauvegardes automatiques
	 */
	private String chAdrFichier;
	
	/**
	 * Stock tous les �v�nements de la frise dans un TreeSet d'�v�nements 
	 */
	private TreeSet<Evenement> chTreeSetEvent;
	
	/**
	 * Constructeur pour cr�er une nouvelle frise depuis le PanelCreation
	 * @param parAnneeDebut Ann�e de d�but de la frise
	 * @param parAnneeFin Ann�e de fin de la frise
	 * @param parPeriode P�riode de la frise
	 * @param parTitre Titre de la frise
	 * @param parAdrFichier Adresse de sauvegarde du fichier
	 */
	public Frise(int parAnneeDebut, int parAnneeFin, int parPeriode, String parTitre, String parAdrFichier) {
		this.chAnneeDebut = parAnneeDebut;
		this.chAnneeFin = parAnneeFin;
		this.chPeriode = parPeriode;
		this.chTitre = parTitre;
		this.chAdrFichier = parAdrFichier;
		this.chTreeSetEvent = new TreeSet<Evenement>();
	}
	
	/**
	 * Ajoute un �v�nement � chTreeSetEvent
	 * @param parEvt L'�v�nement � ajouter
	 */
	public void add(Evenement parEvt) {
		if(parEvt.getChDate().getAnnee() <= this.chAnneeFin && parEvt.getChDate().getAnnee() >= this.chAnneeDebut) {
			chTreeSetEvent.add(parEvt);
			chNombreEvenement++;
		}
	}
	
	/**
	 * Supprime un �v�nement de chTreeSetEvent
	 * @param parString Inserez ici la methode toString() de l'�v�nement � supprimer
	 * @return Renvoie l'�v�nement qui � �t� supprim�
	 */
	public Evenement del(String parString) {
		Evenement evenementARetourner = null;
		for (Evenement evt : chTreeSetEvent) {
			if(evt.toString().equals(parString)) {
				evenementARetourner = evt;
				chTreeSetEvent.remove(evt);
				chNombreEvenement--;
				break; // Ne doit pas continuer � parcourir si la boucle for est alt�r�e par le remove
			}
		}
		return evenementARetourner;
	}
	
	/**
	 * Accesseur qui renvoit l'ann�e de d�but de la frise
	 * @return Renvoit l'ann�e de d�but de la frise
	 */
	public int getAnneeDebut() {
		return chAnneeDebut;
	}
	
	/**
	 * Accesseur qui renvoit l'ann�e de fin de la frise
	 * @return Renvoit l'ann�e de fin de la frise
	 */
	public int getAnneeFin() {
		return chAnneeFin;
	}
	
	/**
	 * Accesseur qui renvoit la p�riode de la frise
	 * @return Renvoit la periode de la frise
	 */
	public int getPeriode() {
		return chPeriode;
	}
	
	/**
	 * Accesseur qui renvoit le titre de la frise
	 * @return Renvoit le titre de la frise
	 */
	public String getTitre() {
		return chTitre;
	}
	
	/**
	 * Accesseur qui renvoit le chemin du fichier
	 * @return Renvoit le chemin du fichier
	 */
	public String getAdrFichier() {
		return chAdrFichier;
	}
	
	/**
	 * Accesseur qui renvoit le TreeSet de tous les �v�nements sauvegard�s dans la frise
	 * @return Renvoit le TreeSet de tous les �v�nements sauvegard�s dans la frise
	 */
	public TreeSet<Evenement> getTreeSetEvent() {
		return chTreeSetEvent;
	}
	
	/**
	 * Accesseur qui renvoit le nombre d'�v�nements contenus dans le TreeSet
	 * @return Renvoit le nombre d'�v�nements contenus dans le TreeSet
	 */
	public int getNombreEvenement() {
		return chNombreEvenement;
	}
	
	/**
	 * Modificateur qui d�finit le chemin du fichier
	 * @param Chemin du fichier dans une String
	 */
	public void setAdrFichier(String chAdrFichier) {
		this.chAdrFichier = chAdrFichier;
	}
}
