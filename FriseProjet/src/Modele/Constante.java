package Modele;

import java.awt.Color;
import java.awt.Font;

public class Constante {
    public static  String NOM_APPLICATION = "Logiciel de Frise";
	public static  String [] TAB_STR_MOIS = {"janvier", "fevrier", "mars", "avril", "mai", "juin", "juillet", "aout", "septembre", "octobre", "novembre", "decembre"};
	public static  String [] TAB_STR_JOURS = {"dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"};
	public static  String [] TAB_STR_JOURS_PETIT = {"di", "lu", "ma", "me", "je", "ve", "sa"};
	public static  String [] TAB_BOUTON = {"<<","<",">",">>"};
    public static  String INTITULE_AJOUT = "Ajout";
    public static  Color COLOR_BACKGROUND = new Color(238, 238, 238);
    public static  Color COLOR_OK = new Color(152,251,152);
    public static  Color COLOR_ERREUR = new Color(255,245,0);
	public static  Font FONT_TITRE = new Font("Arial",Font.PLAIN,30);
}
