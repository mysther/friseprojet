package Modele;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableModel;


public class ModeleTable extends DefaultTableModel {
	private static final long serialVersionUID = 1L;
	/**
	 * Frise sauvegard�e depuis le constructeur @see {@link ModeleTable}
	 */
	private Frise chFrise;
	
	/**
	 * Constructeur de la classe ModeleTable. Prend une frise en param�tre et place ses �v�nements dans le tableau.
	 * @param parFrise Recupere une frise pour afficher ses donn�es
	 */
	public ModeleTable(Frise parFrise) {
		chFrise = parFrise;
		int nombreColonnes = (chFrise.getAnneeFin()-chFrise.getAnneeDebut()+1);
		this.setColumnCount(nombreColonnes/chFrise.getPeriode());
		this.setRowCount(4);
		
		String [] nomColonnes = new String [nombreColonnes];
		for (int i = 0 ; i < nombreColonnes ; i++) {
			if(i % parFrise.getPeriode() == 0) 
				nomColonnes[i] = Integer.toString(chFrise.getAnneeDebut()+i);
			else 
				nomColonnes[i] = "";
		}
		this.setColumnIdentifiers(nomColonnes);
		for(Evenement evenement : chFrise.getTreeSetEvent()) 
			this.addEvenement(evenement);
	}
	
	/**
	 * M�thode permettant d'ajouter un �v�nement.
	 * @param evenement - l'�v�nement � ajouter
	 */
	public void addEvenement(Evenement evenement) {
		int col = evenement.getChDate().getAnnee()-chFrise.getAnneeDebut();
		int row = evenement.getImportance()-1;
		ImageIcon image = new ImageIcon(evenement.getImage().getImage().getScaledInstance(65, 65, Image.SCALE_DEFAULT));
		image.setDescription(evenement.toString());
		this.setValueAt(image, row, col);
	}
	
	/**
	 * M�thode permettant de supprimer un �v�nement.
	 * @param evenement - l'�v�nement � supprimer
	 */
	public void delEvenement(Evenement evenement) {
		int col = evenement.getChDate().getAnnee()-chFrise.getAnneeDebut();
		int row = evenement.getImportance()-1;
		this.setValueAt(null, row, col);
	}

	/**
	 * M�thode de la classe renvoit la classe JTableModel de la colonne souhait�e
	 * @param parNum - Num�ro de la colonne
	 * @return retourne le type de classe
	 */
	@Override
	public Class<?> getColumnClass(int parNum) {
		return ImageIcon.class;
	}
	
	/**
	 * M�thode de la classe JTableModele qui renvoit un bool�en pour savoir si la cellule s�lectionn�e est vide ou non
	 * @return retourne faux si la cellule n'est pas vide (et donc �ditable);
	 */
	@Override
	public boolean isCellEditable(int rowIndex,int columnIndex) {
		return false;
	}
}
