package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import Modele.Date;
import Modele.Evenement;

public class TestEvenement {

	@Test
	public void testCompareTo() {
		Evenement e1 = new Evenement("Titre", new Date(1,1,2000), "Description", "Adresse Image", 1);
		Evenement e2 = new Evenement("Titre", new Date(2,2,2002), "Description", "Adresse Image", 1);
		if(e1.compareTo(e2) >= 0) {
			fail("Evenement param�tre post�rieur");
		}
		
		if(e2.compareTo(e1) <= 0) {
			fail("Evenement param�tre ant�rieur");
		}
		
		if(e2.compareTo(e2) != 0) {
			fail("M�me date");
		}
	}
}
