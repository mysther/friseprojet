package Test;

import static org.junit.Assert.fail;

import org.junit.Test;

import Modele.Date;

public class TestDate {

	@Test
	public void testCompareTo() {
		
		Date d1 = new Date(01,01,2012);
		Date d2 = new Date(01,01,2012);
		
		if(d1.compareTo(d2)!=0){
			fail("M�me date");
		}
		
		d2=new Date(02,01,2012);
		
		if(d1.compareTo(d2)>=0){
			fail("Date param�tre post�rieure");
		}
		
		d2=new Date(31,12,2011);
		
		if(d1.compareTo(d2)<=0){
			fail("Date param�tre ant�rieure");
		}
	}
	


	@Test
	public void testDernierJourDuMois() {
	
		if(Date.dernierJourDuMois(1, 2000) != 31)
			{
			fail("Janvier / ann�e bissextile");
			}
		if(Date.dernierJourDuMois(2, 2000) != 29)
			{
			fail("F�vrier / ann�e bissextile");
			}
		if(Date.dernierJourDuMois(7, 2001) != 31)
			{
			fail("Juillet / ann�e non bissextile");
			}
		if(Date.dernierJourDuMois(2, 2001) != 28)
			{
			fail("F�vrier / ann�e non bissextile");
			}
		if(Date.dernierJourDuMois(9, 2001) != 30)
			{
			fail("Juillet / ann�e non bissextile");
			}
	}

	@Test
	public void testEstValide() {
		Date d1 = new Date(42,01,2012);
		Date d2 = new Date(02,13,2012);
		Date d3 = new Date(01,01,2012);
		Date d4 = new Date(29,02,2001);
		Date d5 = new Date(29,02,2000);
		Date d6 = new Date(31,06,2000);
		
		if(d1.estValide()==true){
			fail("Jour hors limite");
		}
		if(d2.estValide()==true){
			fail("Mois hors limite");
		}
		if(d3.estValide()==false){
			fail("Date banale");
		}
		if(d4.estValide()==true){
			fail("29 Fevrier en ann�e non bissextile");
		}
		if(d5.estValide()==false){
			fail("29 F�vrier en ann�e bissextile");
		}
		if(d6.estValide()==true){
			fail("31 Juin");
		}
	}
}
