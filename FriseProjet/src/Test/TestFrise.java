package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import Modele.Date;
import Modele.Evenement;
import Modele.Frise;

public class TestFrise {

	@Test
	public void testAdd() {
		Frise f1 = new Frise(2000, 2020, 2, "Titre frise", "Adresse sauvegarde");
		Evenement e1 = new Evenement("Titre", new Date(2,2,2002), "Description", "Adresse Image", 1);
		Evenement e2 = new Evenement("Titre", new Date(2,2,1900), "Description", "Adresse Image", 1);
		
		f1.add(e1);
		if(f1.getNombreEvenement()!=1) {
			fail("Ajout evenement");
		}
		
		f1.add(e2);
		if(f1.getNombreEvenement()!=1) {
			fail("Ajout evenement impossible, n'est pas compris dans la date de debut et fin de frise");
		}
	}

	@Test
	public void testDel() {
		Frise f1 = new Frise(2000, 2020, 2, "Titre frise", "Adresse sauvegarde");
		Evenement e1 = new Evenement("Titre", new Date(2,2,2002), "Description", "Adresse Image", 1);
		
		f1.add(e1);
		f1.del(e1.toString());
		if(f1.getNombreEvenement()!=0) {
			fail("Ajout evenement impossible, n'est pas compris dans la date de debut et fin de frise");
		}
	}

}
